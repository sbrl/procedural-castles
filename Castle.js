"use strict";

class Castle
{
	constructor(canvasSize)
	{
		this.canvasSize = canvasSize;
		
		this.pos = canvasSize.clone().divide(2); // The Castle's position.
		// Number of towers
		this.towerCount = random(4, 7);
		// The amount of freedom the towers have to move around.
		this.towerPR = canvasSize.minComponent / 10;
		// The amount of space around the castle.
		this.castleBorderSize = canvasSize.minComponent / 8;
		this.towerRadius = canvasSize.minComponent / 24;
		
		this.grassColour = "#609e47"; // Colour of the grass
		
		this.wallBG = "rgb(116, 104, 61)"; // Wall background colour
		this.wallWidth = canvasSize.minComponent / 34; // Wall width.
		
		/// Wall Battlements
		this.wallEdgeWidth = 8;
		this.wallEdgeLowWidth = 4;
		this.wallEdgeOffset = -10;
		this.wallEdgeBG = "#6e6e6e";
		this.wallEdgeLowBG = "#939393";
		this.wallEdgeStyle = [5, 5];
		
		// The percentage along the wall from the centre of the wall that the
		// entrance towers should be positioned at.
		this.entranceTowerPerc = 0.15;
		
		this.drawbridgeBG = "#70531f";
		this.drawbridgeHighBG = "#3b2b0f";
		this.drawbridgeWidth = 0.1;
		this.drawbridgeLength = canvasSize.minComponent / 7;
		this.drawbridgePlankCount = 10;
		
		this.riverBorder = canvasSize.minComponent / 5;
		this.riverBG = "#3497a4";
		this.riverWidth = canvasSize.minComponent / 20;
		
		// Actually generate the castle.
		this.generate(canvasSize);
	}
	
	generate(canvasSize)
	{
		/// Generate the towers ///
		this.towers = [];
		
		// Work out the points for each tower
		this.shape = new Shape(this.pos.clone(), this.towerCount, (canvasSize.x / 4) - this.castleBorderSize);
		// Randomize the tower location a little bit, and then create the tower
		// itself
		for(let i = 0; i < this.shape.points.length; i++)
		{
			this.shape.points[i].add(new Vector(
				(Math.random() * (this.towerPR)) - (this.towerPR / 2),
				(Math.random() * (this.towerPR)) - (this.towerPR / 2)
			));
			
			this.towers.push(new Tower(canvasSize, this.shape.points[i], this.towerRadius));
		}
		
		
		/// Generate the keep ///
		this.keep = new Tower(canvasSize, this.pos.clone(), Math.min(canvasSize.x, canvasSize.y) / 15, true);
		
		/// Generate the entrance ///
		var longestWall = this.findLongestWall();
		this.entranceTowers = [
			new Tower(canvasSize,
				longestWall.start.clone().moveTowards(longestWall.end, longestWall.length * (0.5 - this.entranceTowerPerc)),
				this.towerRadius * 0.6, false
			),
			new Tower(canvasSize,
				longestWall.start.clone().moveTowards(longestWall.end, longestWall.length * (0.5 + this.entranceTowerPerc)),
				this.towerRadius * 0.6, false
			)
		];
		
		/// Generate the drawbridge ///
		this.drawbridgePos = longestWall.start.clone().moveTowards(longestWall.end, longestWall.length * 0.5);
		this.drawbridgeWidth *= longestWall.length;
		
		/// Generate the moat ///
		this.generateRiver();
		
		/// Generate a bunch of paths ///
	}
	
	findLongestWall()
	{
		var longest = {
			length: 0,
			start: null, end: null
		};
		
		for(let i = 1; i <= this.towers.length; i++)
		{
			let prevVector = this.towers[i - 1].pos.clone(),
				curVector = this.towers[0].pos.clone();
			if(i !== this.towers.length)
				curVector = this.towers[i].pos.clone();
			
			let nextLength = prevVector.clone().subtract(curVector).length;
			if(nextLength > longest.length)
			{
				longest.length = nextLength;
				longest.start = prevVector;
				longest.end = curVector;
			}
		}
		
		return longest;
	}
	
	generateRiver()
	{
		var longest = this.findLongestWall();
		this.river = new SmoothLine();
		for(let i = 0; i < this.towers.length; i++)
		{
			this.river.add(this.towers[i].pos.clone().moveTowards(this.keep.pos, -this.riverBorder));
			
			let curPos = this.towers[i].pos,
				nextPos = i == this.towers.length - 1 ? this.towers[0].pos : this.towers[i + 1].pos;
			if(curPos.equalTo(longest.start) && nextPos.equalTo(longest.end))
			{
				for(let tower of this.entranceTowers)
				{
					this.river.add(tower.pos.clone().moveTowards(this.keep.pos, -this.riverBorder/2));
				}
			}
		}
		var interpolatedPoints = this.river.interpolateOnce(this.river.points, 0.5);
		// this.river.add(this.river.points[0]); // Close the river off to form a moat
		// this.river.add(interpolatedPoints[0]); // Close the river off to form a moat
		this.river.close();
	}
	
	placeBuildings()
	{
		
	}
	
	generatePaths()
	{
		
	}
	
	nextFrame()
	{
		this.update();
		this.render(this.context);
		
		//requestAnimationFrame(this.nextFrame.bind(this));
	}
	
	update()
	{
		
	}
	
	render(context)
	{
		context.fillStyle = this.grassColour;
		context.fillRect(0, 0, this.canvasSize.x, this.canvasSize.y);
		
		this.keep.render(context);
		
		this.drawRiver(context);
		
		// Draw the drawbridge
		this.drawDrawbridge(context);
		
		// Draw the walls
		for(let i = 1; i <= this.towers.length; i++)
		{
			let prevVector = this.towers[i - 1].pos.clone(),
				curVector = this.towers[0].pos.clone();
			if(i !== this.towers.length)
				curVector = this.towers[i].pos.clone();
			this.drawWall(context, prevVector, curVector);
		}
		
		// Draw the towers
		for(let i = 0; i < this.towers.length; i++)
		{
			this.towers[i].render(context);
		}
		
		// Draw the entrance towers
		this.drawEntrance(context);
	}
	
	drawWall(context, start, end)
	{
		context.save();
		context.translate(start.x, start.y);
		context.rotate(start.angleFrom(end));
		
		var lineLength = end.clone().subtract(start).length;
		
		// The wall itself
		context.strokeStyle = this.wallBG;
		context.lineWidth = this.wallWidth;
		
		context.beginPath();
		context.moveTo(0, 0);
		context.lineTo(lineLength, 0);
		context.stroke();
		
		// The battlements
		context.lineWidth = this.wallEdgeWidth;
		context.strokeStyle = this.wallEdgeBG;
		context.setLineDash(this.wallEdgeStyle);
		
		context.beginPath();
		context.moveTo(0, this.wallEdgeOffset);
		context.lineTo(lineLength, this.wallEdgeOffset);
		context.stroke();
		
		context.lineDashOffset = this.wallEdgeStyle[0];
		context.strokeStyle = this.wallEdgeLowBG;
		context.lineWidth = this.wallEdgeLowWidth;
		
		context.stroke();
		
		context.restore();
	}
	
	drawEntrance(context)
	{
		for(let i = 0; i < this.entranceTowers.length; i++)
		{
			this.entranceTowers[i].render(context);
		}
	}
	
	drawDrawbridge(context)
	{
		context.save();
		context.translate(this.drawbridgePos.x, this.drawbridgePos.y);
		context.rotate(this.entranceTowers[0].pos.angleFrom(this.entranceTowers[1].pos));
		context.translate(-this.drawbridgeWidth/2, 0);
		
		context.fillStyle = this.drawbridgeBG;
		context.fillRect(0, 0, this.drawbridgeWidth, -this.drawbridgeLength);
		
		context.translate(0, -this.drawbridgeLength);
		var plankLength = this.drawbridgeLength / (this.drawbridgePlankCount * 2);
		context.fillStyle = this.drawbridgeHighBG;
		for(let i = this.drawbridgeLength; i > 0; i -= plankLength * 2)
		{
			context.fillRect(0, i, this.drawbridgeWidth, plankLength);
		}
		
		context.restore();
	}
	
	drawRiver(context)
	{
		context.save();
		
		context.strokeStyle = this.riverBG;
		context.lineWidth = this.riverWidth;
		
		context.beginPath();
		this.river.line(context, 16);
		context.stroke();
		
		/// Debug drawing ///
		// var i = 0;
		// for(let point of this.river.points)
		// {
		// 	Debug.shape(context, point, "circle", 10, "red");
		// 	context.fillStyle = "black";
		// 	context.fillText(i.toString(), point.x, point.y);
		// 	i++;
		// }
		// 
		// var j = 0;
		// for(let point of this.river.interpolatedPoints)
		// {
		// 	Debug.shape(context, point, "circle", 10, "orange");
		// 	context.fillStyle = "black";
		// 	context.fillText(j.toString(), point.x, point.y);
		// 	j++;
		// }
		
		context.restore();
	}
}
