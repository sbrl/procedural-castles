/**
 * Bounded random number generator. Has 3 forms:
 *
 ** Form 1 **
 * @param  {number}  a The minimum value.
 * @param  {number}  b The maximum value.
 * @param  {boolean} c Whether the resulting number should be a float. [optional]
 * @return {number}    A random number.
 * 
 ** Form 2 **
 * @param  {number}  a The maximum value.
 * @param  {boolean} b Whether the resulting number should be a float. [optional]
 * @return {number}    A random number.
 * 
 ** Form 3 **
 * @return {number}    A random number.
 */
function random(a,b,c,d)
{d=Math.random();if(typeof a!="number")return d;a=typeof b=="number"?d*(a-b)+b:d*a;a=(typeof b!="number"?b:c)?a:Math.floor(a);return a;}
