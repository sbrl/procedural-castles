# Procedural Castles
This is a procedural castle generator that I've built for a [challenge](https://www.reddit.com/r/proceduralgeneration/comments/3zy53l/monthly_challenge_2_jan_2016_procedural_castle/) over at [/r/proceduralgeneration](//reddit.com/r/proceduralgeneration). It's built in pure HTML5 + ES6.

**Demo:** [GitLab Pages](https://sbrl.gitlab.io/procedural-castles/)

## Update 1 (31st Jan 2016)
I've got wall and tower generation online.

![a](http://i.imgur.com/2CQO213.png)
![b](http://i.imgur.com/3GDg6qy.png)
![c](http://i.imgur.com/nfR1UNj.png)

## Update 2 (31st Jan 2016)
I've done some more work, and I now have the entrance towers and a drawbridge hooked up. I've also reduced the amount of freedom that the towers have to move around in, so you should see any towers on top of each other any more. If possible, I want to draw a moat too soon.

![a](http://i.imgur.com/nCRkP78.png)
![b](http://i.imgur.com/CPWPo3N.png)
![c](http://i.imgur.com/7FiMmvr.png)

## Update 3 (31st Jan 2016)
Having a drawbridge without a moat is a little bit silly, so I've hacked my [SmoothLine](https://gist.github.com/sbrl/ed97524db09481e1b018) class and added one. 

![a](http://i.imgur.com/j6UIMHb.png)
