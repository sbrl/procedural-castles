"use strict";

class Building
{
	constructor(pos, size, rotation)
	{
		this.pos = pos;
		this.size = size;
		this.rotation = rotation;
		
		// rgb(76, 61, 222) - nice blue, wizard?
		// hsl(21, 59%, 26%) - Original Roof BG
		this.roofBG = `hsl(${random(16, 26)}, ${random(56, 63)}%, ${random(23, 30)}%)`;
		// TODO: Add some random variation to this
		this.roofHighBG = `hsl(32, 85%, 35%)`;
	}
	
	render(context)
	{
		context.save();
		context.translate(this.pos.x, this.pos.y);
		context.rotate(this.rotation);
		
		this.fillStyle = this.roofBG;
		this.fillRect(0, 0, this.size.x, this.size.y);
		
		context.restore();
	}
}
