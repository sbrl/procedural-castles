"use strict";

window.Debug = {
	shape: function(context, pos, type, radius, colour) {
		context.beginPath();
		switch(type)
		{
			case "circle":
				context.ellipse(pos.x, pos.y, radius, radius, 0, 0, Math.PI * 2, false);
				break;
		}
		
		context.fillStyle = colour;
		context.fill();
	}
}
