"use strict";

class Renderer
{
	constructor(canvas)
	{
		this.canvas = canvas;
		this.context = canvas.getContext("2d");
		
		// Make the canvas the saame size as the screen
		this.trackWindowSize();
		
		// Create the castle
		this.castle = new Castle(new Vector(this.canvas.width, this.canvas.height));
	}
	
	nextFrame()
	{
		this.update();
		this.render(this.canvas, this.context);
		// requestAnimationFrame(this.nextFrame.bind(this));
	}
	
	update()
	{
		
	}
	
	render(canvas, context)
	{
		context.clearRect(0, 0, canvas.width, canvas.height);
		
		this.castle.render(context);
	}
	
	/**
	 * Updates the canvas size to match the current viewport size.
	 */
	matchWindowSize() {
		this.canvas.width = window.innerWidth;
		this.canvas.height = window.innerHeight;
		
		//this.render(this.context);
	}
	
	/**
	 * Makes the canvas size track the window size.
	 */
	trackWindowSize() {
		this.matchWindowSize();
		window.addEventListener("resize", this.matchWindowSize.bind(this));
	}
}

window.addEventListener("load", function (event) {
	var canvas = document.getElementById("canvas-main"),
		renderer = new Renderer(canvas);
	renderer.nextFrame();
	window.renderer = renderer;
});
