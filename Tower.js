"use strict";

class Tower
{
	constructor(canvasSize, pos, radius, isKeep)
	{
		this.isKeep = typeof isKeep == "boolean" ? isKeep : false;
		// The position of the tower
		this.pos = pos;
		// The radius of the tower
		this.towerRadius = radius;
		
		this.towerBG = "#917853";
		this.wallBG = "#6e6e6e";
		this.wallStyle = [5, 5];
		this.wallWidth = 10;
		
		this.wallLowBG = "#939393";
		this.wallLowWidth = 5;
		
		this.stairBG = "#625027";
		this.stairWidth = 6;
		this.stairStepCount = 4;
		this.stairPosRadius = 0.6;
		
		this.flagPoleBG = "#535353";
		this.flagPoleRadius = 3;
		this.flagLength = 15;
		this.flagWidth = 4;
		
		this.keepFlagPosRadius = 0.5;
		this.keepFlagCount = random(3, 7);
	}
	
	update()
	{
		
	}
	
	render(context)
	{
		context.save();
		context.translate(this.pos.x, this.pos.y);
		
		this.drawTower(context);
		
		// Debug.shape(context, new Vector(0, 0), "circle", 6, "rgb(16, 142, 28)");
		
		context.restore();
	}
	
	drawTower(context)
	{
		context.save();
		context.save();
		
		context.fillStyle = this.towerBG;
		context.strokeStyle = this.wallBG;
		context.lineWidth = this.wallWidth;
		context.setLineDash(this.wallStyle);
		
		context.beginPath();
		context.ellipse(0, 0, this.towerRadius, this.towerRadius, 0, 0, Math.PI * 2, false);
		context.fill();
		context.stroke();
		
		
		context.strokeStyle = this.wallLowBG;
		context.lineDashOffset = 5;
		context.lineWidth = this.wallLowWidth;
		
		context.beginPath();
		context.ellipse(0, 0, this.towerRadius, this.towerRadius, 0, 0, Math.PI * 2, false);
		context.stroke();
		
		context.restore();
		
		if(!this.isKeep)
			this.drawStairs(context);
		if(!this.isKeep)
		{
			this.drawFlagPole(context, new Vector(0, 0));
		}
		else
		{
			let radius = this.towerRadius * this.keepFlagPosRadius;
			for(let n = 0; n < this.keepFlagCount; n++)
			{
				this.drawFlagPole(context, new Vector(
					radius * Math.cos(Math.PI*2 * (n / this.keepFlagCount) - Math.PI/2),
					radius * Math.sin(Math.PI*2 * (n / this.keepFlagCount) - Math.PI/2)
				));
			}
		}
		
		context.restore();
	}
	
	drawStairs(context)
	{
		context.save();
		
		var stairRad = this.towerRadius * this.stairPosRadius,
			stairRotation = random(0, Math.PI * 2, true);
		
		context.strokeStyle = this.stairBG;
		context.lineWidth = this.stairWidth;
		
		context.beginPath();
		context.ellipse(0, 0, stairRad, stairRad, stairRotation, 0, Math.PI / 2.1, false);
		context.stroke();
		
		context.strokeStyle = "rgba(0, 0, 0, 0.1)";
		for(let i = 0; i < 1; i += 1 / this.stairStepCount)
		{
			context.beginPath();
			context.ellipse(0, 0, stairRad, stairRad, stairRotation, 0, (Math.PI / 2.1) * i, false);
			
			context.stroke();
		}
		
		context.restore();
	}
	
	drawFlagPole(context, pos)
	{
		context.save();
		context.translate(pos.x, pos.y);
		context.rotate(Tower.flagRotation);
		
		context.fillStyle = this.flagPoleBG;
		
		context.beginPath();
		context.ellipse(0, 0, this.flagPoleRadius, this.flagPoleRadius, 0, 0, Math.PI * 2, false);
		context.fill();
		
		// Draw the flag itself
		context.strokeStyle = Tower.flagBG;
		context.lineWidth = this.flagWidth;
		
		context.beginPath();
		context.moveTo(0, 0);
		context.lineTo(this.flagLength, 0);		
		context.stroke();
		
		context.restore();
	}
}
Tower.flagBG = `hsl(${random(0, 255)}, 59%, 50%)`;
Tower.flagRotation = random(0, Math.PI * 2, true);
